# All in one

React & node.js (express) pet project whose primary purpose is to show skills working with React and node.js, and corresponding instruments such as:
- typescript
- redux toolkit
- final-form
- axios
- dayjs
- styled-components
